<table class="table table-responsive" id="libros-table">
    <thead>
        <th>Cod Libro</th>
        <th>Nombre</th>
        <th>Autor</th>
        <th>Anho</th>
        <th>Pdf</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($libros as $libros)
        <tr>
            <td>{!! $libros->cod_libro !!}</td>
            <td>{!! $libros->nombre !!}</td>
            <td>{!! $libros->autor !!}</td>
            <td>{!! $libros->anho !!}</td>
            <td>{!! $libros->pdf !!}</td>
            <td>
                {!! Form::open(['route' => ['libros.destroy', $libros->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('libros.show', [$libros->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('libros.edit', [$libros->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>