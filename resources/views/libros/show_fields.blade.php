<div class="col-md-6">
    
<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $libros->id !!}</p>
</div>

<!-- Cod Libro Field -->
<div class="form-group">
    {!! Form::label('cod_libro', 'Cod Libro:') !!}
    <p>{!! $libros->cod_libro !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $libros->nombre !!}</p>
</div>

<!-- Autor Field -->
<div class="form-group">
    {!! Form::label('autor', 'Autor:') !!}
    <p>{!! $libros->autor !!}</p>
</div>

<!-- Anho Field -->
<div class="form-group">
    {!! Form::label('anho', 'Anho:') !!}
    <p>{!! $libros->anho !!}</p>
</div>

<!-- Pdf Field -->
<div class="form-group">
    {!! Form::label('pdf', 'Pdf:') !!}
    <p>{!! $libros->pdf !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $libros->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $libros->updated_at !!}</p>
</div>
</div>
<div class="col-md-6">
<div class="form-group">
    <embed src="{!!asset('storage/solicitud-tarjeta-respaldada.pdf')!!}" width="500" height="500">
</div>
</div>



