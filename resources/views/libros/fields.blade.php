<!-- Cod Libro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cod_libro', 'Cod Libro:') !!}
    {!! Form::text('cod_libro', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Autor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('autor', 'Autor:') !!}
    {!! Form::text('autor', null, ['class' => 'form-control']) !!}
</div>

<!-- Anho Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anho', 'Anho:') !!}
    {!! Form::text('anho', null, ['class' => 'form-control']) !!}
</div>

<!-- Pdf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pdf', 'Pdf:') !!}
  
</div>

<div class="form-group col-sm-6">
    <label class="col-md-4 control-label">Nuevo Archivo</label>
    <input type="file" class="form-control" name="pdf" >   
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('libros.index') !!}" class="btn btn-default">Cancel</a>
</div>
