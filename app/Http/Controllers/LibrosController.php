<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLibrosRequest;
use App\Http\Requests\UpdateLibrosRequest;
use App\Repositories\LibrosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Libros;

class LibrosController extends AppBaseController
{
    /** @var  LibrosRepository */
    private $librosRepository;

    public function __construct(LibrosRepository $librosRepo)
    {
        $this->librosRepository = $librosRepo;
    }

    /**
     * Display a listing of the Libros.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->librosRepository->pushCriteria(new RequestCriteria($request));
        $libros = $this->librosRepository->all();

        return view('libros.index')
            ->with('libros', $libros);
    }

    /**
     * Show the form for creating a new Libros.
     *
     * @return Response
     */
    public function create()
    {
        return view('libros.create');
    }

    /**
     * Store a newly created Libros in storage.
     *
     * @param CreateLibrosRequest $request
     *
     * @return Response
     */
    public function store(CreateLibrosRequest $request)
    {
        //$input = $request->all();

        //$libros = $this->librosRepository->create($input);
        
        $libros = new Libros;

        $libros->cod_libro = $request->cod_libro;
        $libros->nombre = $request->nombre;
        $libros->autor = $request->autor;
        $libros->anho = $request->anho;

        //obtenemos el campo file definido en el formulario
        $file = $request->file('pdf');
        //obtenemos el nombre del archivo
        $nombre = $file->getClientOriginalName();

        $libros->pdf = $file->getClientOriginalName();
        $libros->save();
    
        //indicamos que queremos guardar un nuevo archivo en el disco local
        \Storage::disk('local')->put($nombre,  \File::get($file));

        Flash::success('Libros saved successfully.');

        return redirect(route('libros.index'));
    }

    /**
     * Display the specified Libros.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $libros = $this->librosRepository->findWithoutFail($id);

        if (empty($libros)) {
            Flash::error('Libros not found');

            return redirect(route('libros.index'));
        }

        return view('libros.show')->with('libros', $libros);
    }

    /**
     * Show the form for editing the specified Libros.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $libros = $this->librosRepository->findWithoutFail($id);

        if (empty($libros)) {
            Flash::error('Libros not found');

            return redirect(route('libros.index'));
        }

        return view('libros.edit')->with('libros', $libros);
    }

    /**
     * Update the specified Libros in storage.
     *
     * @param  int              $id
     * @param UpdateLibrosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLibrosRequest $request)
    {
        $libros = $this->librosRepository->findWithoutFail($id);

        if (empty($libros)) {
            Flash::error('Libros not found');

            return redirect(route('libros.index'));
        }

        $libros = $this->librosRepository->update($request->all(), $id);

        Flash::success('Libros updated successfully.');

        return redirect(route('libros.index'));
    }

    /**
     * Remove the specified Libros from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $libros = $this->librosRepository->findWithoutFail($id);

        if (empty($libros)) {
            Flash::error('Libros not found');

            return redirect(route('libros.index'));
        }

        $this->librosRepository->delete($id);

        Flash::success('Libros deleted successfully.');

        return redirect(route('libros.index'));
    }
}
