<?php

namespace App\Repositories;

use App\Models\Libros;
use InfyOm\Generator\Common\BaseRepository;

class LibrosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cod_libro',
        'nombre',
        'autor',
        'anho'
        //'pdf'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Libros::class;
    }
}
