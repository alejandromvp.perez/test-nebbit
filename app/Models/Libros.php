<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Libros
 * @package App\Models
 * @version July 17, 2018, 12:45 am UTC
 */
class Libros extends Model
{
    use SoftDeletes;

    public $table = 'libros';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'cod_libro',
        'nombre',
        'autor',
        'anho',
        'pdf'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'cod_libro' => 'string',
        'nombre' => 'string',
        'autor' => 'string',
        'anho' => 'string',
        'pdf' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cod_libro' => 'required',
        'nombre' => 'required',
        'autor' => 'required',
        'anho' => 'numeric'
    ];

    
}
